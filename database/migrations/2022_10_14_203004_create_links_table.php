<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links', function (Blueprint $table) {
            $table->id();
            $table->string('link', 255);
            $table->string('token', 8)->unique();
            $table->integer('redirect_count')->default(0);
            $table->integer('already_redirected_count')->default(0);
            $table->dateTime('expires_at');
            $table->timestamps();

            $table->index(['token']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links');
    }
};

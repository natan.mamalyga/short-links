<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    /**
     * @var string[]
     */
    protected $fillable = [
        'redirect_count',
        'expires_at',
        'already_redirected_count',
    ];

    /**
     * @var string[]
     */
    protected $dates = [
        'expires_at',
        'created_at',
        'updated_at',
    ];

    /**
     * @param string $token
     * @return mixed
     */
    public function getLinkByToken(string $token): mixed
    {
        return self::where('token', $token);
    }

    /**
     * @param array $data
     * @return string
     */
    public function createNewLink(array $data): string
    {
        $model = self::insert($data);

        if ($model) {
            return $data['token'];
        }

        return false;
    }

}

<?php

namespace App\Providers;

use App\Http\Services\DateTimeService;
use App\Http\Services\Interfaces\DateTimeServiceInterface;
use App\Http\Services\Interfaces\LinkServiceInterface;
use App\Http\Services\LinkService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            LinkServiceInterface::class,
            LinkService::class
        );

        $this->app->bind(
            DateTimeServiceInterface::class,
            DateTimeService::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\LinkRequest;
use App\Http\Services\Interfaces\LinkServiceInterface;
use Illuminate\Http\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

class LinkController extends Controller
{
    /**
     * @var LinkServiceInterface
     */
    protected LinkServiceInterface $linkService;

    /**
     * @param LinkServiceInterface $linkService
     */
    public function __construct(LinkServiceInterface $linkService)
    {
        $this->linkService = $linkService;
    }

    /**
     * @Route send-start-link
     *
     * @param LinkRequest $request
     * @return RedirectResponse
     */
    public function create(LinkRequest $request): RedirectResponse
    {
        $data = $this->linkService->normalizeLinkData($request->all());

        $shortUrl = $this->linkService->createNewLink($data);

        return redirect()->back()->withSuccess(url('/' . $shortUrl));
    }

    /**
     * @Route redirectToCustomUrl
     *
     * @param string $token
     * @return RedirectResponse
     */
    public function redirectToCustomUrl(string $token): RedirectResponse
    {
        $link = $this->linkService->getLinkByToken($token);

        return $link ? redirect()->to($link) : redirect()->route('error404');
    }
}

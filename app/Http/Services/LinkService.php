<?php

namespace App\Http\Services;

use App\Http\Services\Interfaces\DateTimeServiceInterface;
use App\Http\Services\Interfaces\LinkServiceInterface;
use App\Models\Link;
use Carbon\Carbon;

class LinkService implements LinkServiceInterface
{
    /**
     * @var Link
     */
    protected Link $linkModel;

    /**
     * @var DateTimeServiceInterface
     */
    protected DateTimeServiceInterface $dateTimeService;

    /**
     * @param DateTimeServiceInterface $dateTimeService
     */
    public function __construct(DateTimeServiceInterface $dateTimeService)
    {
        $this->linkModel = new Link();
        $this->dateTimeService = $dateTimeService;
    }

    /**
     * @param array $data
     * @return array
     */
    public function normalizeLinkData(array $data): array
    {
        $now = $this->dateTimeService->getNow();

        $data['expires_at'] = $this->dateTimeService->getExpiresAt($data['expires_after']);
        $data['created_at'] = $now;
        $data['updated_at'] = $now;
        $data['token'] = $this->createLinkToken();

        unset($data['_token']);
        unset($data['expires_after']);

        return $data;
    }

    /**
     * @return string
     */
    public function createLinkToken(): string
    {
        $characters = implode(
            '',
            array_merge(
                range(0, 9),
                range('a', 'z'),
                range('A', 'Z')
            )
        );

        $randomString = '';
        for ($i = 0; $i < 8; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }

        $tokenUnique = $this->checkUniqueToken($randomString);

        if ($tokenUnique) {
            return $randomString;
        }

        return $this->createLinkToken();
    }

    /**
     * @param string $token
     * @return bool
     */
    public function checkUniqueToken(string $token): bool
    {
        return !$this->linkModel->getLinkByToken($token)->first();
    }

    /**
     * @param array $data
     * @return string
     */
    public function createNewLink(array $data): string
    {
        return $this->linkModel->createNewLink($data);
    }

    /**
     * @param string $token
     * @return mixed
     */
    public function getLinkByToken(string $token): mixed
    {
        $model = $this->linkModel->getLinkByToken($token)->first();

        $check = $this->checkModelForRelevance($model);

        if ($check) {
            if ($model->redirect_count != 0) {
                $model->increment('already_redirected_count');
            }
            return $model->link;
        }

        return false;
    }

    /**
     * @param $model
     * @return bool
     */
    public function checkModelForRelevance($model): bool
    {
        if (!$model
            || $model->expires_at->lte(Carbon::now())
            || $model->already_redirected_count >= $model->redirect_count
        ) {
            return false;
        }

        return true;
    }
}

<?php

namespace App\Http\Services;

use App\Http\Services\Interfaces\DateTimeServiceInterface;
use Carbon\Carbon;

class DateTimeService implements DateTimeServiceInterface
{
    /**
     * @param $hours
     * @return string
     */
    public function getExpiresAt($hours): string
    {
        return Carbon::now()->addHours($hours)->format('Y-m-d H:i:s');
    }

    /**
     * @return string
     */
    public function getNow(): string
    {
        return Carbon::now()->format('Y-m-d H:i:s');
    }
}

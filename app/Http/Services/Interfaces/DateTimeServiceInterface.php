<?php

namespace App\Http\Services\Interfaces;

interface DateTimeServiceInterface
{
    /**
     * @param $hours
     * @return string
     */
    public function getExpiresAt($hours): string;

    /**
     * @return string
     */
    public function getNow(): string;
}

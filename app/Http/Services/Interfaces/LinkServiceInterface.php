<?php

namespace App\Http\Services\Interfaces;

interface LinkServiceInterface
{
    /**
     * @param array $data
     * @return array
     */
    public function normalizeLinkData(array $data): array;

    /**
     * @return string
     */
    public function createLinkToken(): string;

    /**
     * @param array $data
     * @return string
     */
    public function createNewLink(array $data): string;

    /**
     * @param string $token
     * @return mixed
     */
    public function getLinkByToken(string $token): mixed;
}

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::post('/create-link', [\App\Http\Controllers\LinkController::class, 'create'])
    ->name('createLink');

//we can call controller, but for this task - that's enough
Route::get('/error-404', function (){
    return view('404');
})->name('error404');

Route::get('/{token}', [\App\Http\Controllers\LinkController::class, 'redirectToCustomUrl'])
    ->where('token', '[0-9A-Za-z]+')
    ->name('redirectToCustomUrl');

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <link rel="stylesheet" href="{{asset('css/style.css')}}">

    </head>
    <body>
        <div class="login-box">
            <h2>Test</h2>
            <form class="ajaxForm" method="post" action="{{route('createLink')}}">
                @csrf
                <div class="user-box">
                    <input type="text" name="link" required="">
                    <label>Start link</label>
                </div>
                <div class="user-box">
                    <input type="number" name="expires_after" required="" min="1" max="24" value="1">
                    <label>Expires after N hours</label>
                </div>

                <div class="user-box">
                    <input type="number" name="redirect_count" required="" min="0" value="0">
                    <label>Redirect count( 0 - unlimited )</label>
                </div>

                @if(session('success'))
                    <div class="result">
                        Your short link: {{ session()->get('success') }}
                    </div>
                @endif

                <button type="submit">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    Submit
                </button>
            </form>
        </div>
    </body>

</html>
